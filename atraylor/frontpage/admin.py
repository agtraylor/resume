from django.contrib import admin
from .models import Section, Content

# Register your models here.

class SectionAdmin(admin.ModelAdmin):
    list_display = ('title', 'order')
    search_fields = ['title']

class ContentAdmin(admin.ModelAdmin):
    list_display = ('title', 'parent', 'order', 'slug', 'body', 'last_update_datetime', 'creation_datetime', 'state')
    search_fields = ['title', 'parent', 'body']

admin.site.register(Section, SectionAdmin)
admin.site.register(Content, ContentAdmin)