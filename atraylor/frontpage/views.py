from django.utils import timezone
from django.shortcuts import render
from django.views.generic.list import ListView
from .models import Section, Content

# Create your views here.
class SectionList(ListView):
    model = Section