from django.db import models

# Create your models here.
CONTENT_STATE = ((0, 'Editing'),
                 (1, 'Published'))
                                                                                                                                                                             
class Section(models.Model):
    title = models.CharField(verbose_name='Title', max_length=200)
    order = models.IntegerField(verbose_name='Order', default=999)
    
    class Meta:
        ordering = ['order']

class Content(models.Model):
    parent = models.ForeignKey(Section, on_delete=models.CASCADE)
    title = models.CharField(verbose_name='Title', max_length=200, unique=True)
    order = models.IntegerField(verbose_name='Order')
    slug = models.CharField(verbose_name='Slug', max_length=200, unique=True)
    peek = models.IntegerField(verbose_name='Number Of Characters To Preview')
    body = models.TextField(verbose_name='Body')
    creation_datetime = models.DateTimeField(verbose_name='Creation Datetime', auto_now_add=True)
    last_update_datetime = models.DateTimeField(verbose_name='Last Update Datetime', auto_now=True)
    state = models.IntegerField(verbose_name='State', choices=CONTENT_STATE, default=0)

    class Meta:
        ordering = ['order']