var a;
var lightbanner;
var darkbanner;

window.onload = initPage;


function initPage(e) {
    const toggleSwitch = document.querySelector('input[type="checkbox"]');
    var a = document.getElementsByClassName("more_less")
    var cookiestring=RegExp("mode=[^;]+").exec(document.cookie);
    var mode = decodeURIComponent(!!cookiestring ? cookiestring.toString().replace(/^[^=]+./,"") : "");

    toggleSwitch.addEventListener('change', switchTheme, false);
    

    // Initialize Modal
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);

    // Check dark mode cookie and set display mode
    if(mode == "dark"){
        document.documentElement.setAttribute('data-theme', 'dark');
        document.getElementById("banner").src= darkbanner;
        document.cookie = "mode=dark";
        toggleSwitch.checked = true;
    };

    // Initialize readmore
    for(var i=0;i<a.length;i++)
        if(a[i] != null){
            a[i].addEventListener('click', readMore)
        };

    // Initialize tabs
    var el = document.querySelector('.tabs');
    var instance = M.Tabs.init(el, {swipeable: true});
};


function readMore(e) {
    var content_id = "content_body" + this.id;
    var full_content = document.getElementById("full" + this.id).innerHTML;

    document.getElementById(content_id).innerHTML = full_content;
    return false;
};


function switchTheme(e) {
    if (e.target.checked) {
        document.documentElement.setAttribute('data-theme', 'dark');
        document.getElementById("banner").src= darkbanner;
        document.cookie = "mode=dark";
    } else {
        document.documentElement.setAttribute('data-theme', 'light');
        document.getElementById("banner").src= lightbanner;
        document.cookie = "mode=light";
    };    
};
